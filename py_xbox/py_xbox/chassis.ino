#include <ArduinoJson.h>

#define Motor_left_pin_R_EN 50
#define Motor_left_pin_L_EN 46
#define Motor_left_pin_RPWM 6
#define Motor_left_pin_LPWM 7

#define Motor_right_pin_R_EN 42
#define Motor_right_pin_L_EN 38
#define Motor_right_pin_RPWM 4
#define Motor_right_pin_LPWM 5

#define Led 2

int Speed = 0;

void ShowLed (int pin){
  switch(Speed) {
    case 0: {
        pinMode(pin, LOW);
    }
    break;
    case 1: {
        pinMode(pin, HIGH);
        delay(100);
        pinMode(pin, LOW);
    }
    break;
    case 2: {
        pinMode(pin, HIGH);
        delay(100);
        pinMode(pin, LOW);
        delay(100);
        pinMode(pin, HIGH);
        delay(100);
        pinMode(pin, LOW);
    }
    break;
    case 3: {
        pinMode(pin, HIGH);
        delay(100);
        pinMode(pin, LOW);
        delay(100);
        pinMode(pin, HIGH);
        delay(100);
        pinMode(pin, LOW);
        delay(100);
        pinMode(pin, HIGH);
        delay(100);
        pinMode(pin, LOW);
    }
    break;
    default: {
        pinMode(pin, HIGH);
    }
  }
}

void setup() {
    pinMode(Motor_left_pin_R_EN, OUTPUT);
    pinMode(Motor_left_pin_L_EN, OUTPUT);
    pinMode(Motor_left_pin_RPWM, OUTPUT);
    pinMode(Motor_left_pin_LPWM, OUTPUT);

    pinMode(Motor_right_pin_R_EN, OUTPUT);
    pinMode(Motor_right_pin_L_EN, OUTPUT);
    pinMode(Motor_right_pin_RPWM, OUTPUT);
    pinMode(Motor_right_pin_LPWM, OUTPUT);

    pinMode(Led, OUTPUT);  
    
    Serial.begin(9600); 
    while (!Serial) continue;

    digitalWrite(Led, HIGH);
    delay(100);
    digitalWrite(Led, LOW);
}

void loop() {
    if (Serial.available()) 
    {
        StaticJsonDocument<500> doc;

        DeserializationError err = deserializeJson(doc, Serial);

        if (err == DeserializationError::Ok) 
        {
            digitalWrite(Motor_left_pin_L_EN, doc["Motor_left_L_EN"]);
            digitalWrite(Motor_left_pin_R_EN, doc["Motor_left_R_EN"]); 
                
            digitalWrite(Motor_right_pin_L_EN, doc["Motor_right_L_EN"]);
            digitalWrite(Motor_right_pin_R_EN, doc["Motor_right_R_EN"]);
            
            analogWrite(Motor_left_pin_LPWM, doc["Motor_left_LPWM"]);
            analogWrite(Motor_left_pin_RPWM, doc["Motor_left_RPWM"]);
            
            analogWrite(Motor_right_pin_LPWM, doc["Motor_right_LPWM"]);
            analogWrite(Motor_right_pin_RPWM, doc["Motor_right_RPWM"]); 

            if (doc["block"].as<bool>()) {
              digitalWrite(Led, LOW);
            } else {
              digitalWrite(Led, HIGH);
            }
        } 
    }
  
    delay(20);
}