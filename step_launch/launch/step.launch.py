from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()

    chassis_sub = Node(
        package="py_xbox",
        executable="xbox_sub",
    )

    pad_status = Node(
        package="py_xbox",
        executable="pad_status",
    )

    joy_node = Node(
        package="joy",
        executable="joy_node",
        arguments=["--ros-args --remap _dev_name:=/dev/input/js0"]
    )

    disinfection_sub = Node(
        package="py_disinfection",
        executable="disinfection_sub",
    )

    disinfection_status = Node(
        package="py_disinfection",
        executable="disinfection_status",
    )

    # web = Node(
    #     package="rosbridge_server",
    #     executable="rosbridge_websocket",
    # )

    # Chassis Nodes
    ld.add_action(chassis_sub)
    ld.add_action(pad_status)
    ld.add_action(joy_node)

    # Disinfectuion Nodes
    ld.add_action(disinfection_sub)
    ld.add_action(disinfection_status)

    # ld.add_action(web)

    return ld