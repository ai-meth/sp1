import rclpy
from rclpy.node import Node

from std_msgs.msg import Bool

import pyfirmata

BOARD = pyfirmata.Arduino('/dev/ttyACM0')
print("Communication Successfully started")

class Subscriber(Node):

    def __init__(self):
        super().__init__('sub')
        self.subscription1 = self.create_subscription(Bool, 'disinfection_valve1_sub', self.listener1_callback, 1)
        self.subscription2 = self.create_subscription(Bool, 'disinfection_valve2_sub', self.listener2_callback, 1)
        self.subscription1
        self.subscription2
        self.valve1 = False
        self.valve2 = False
        self.publisher_ = self.create_publisher(Bool, 'disinfection_ardu_ros_pub', 1)
        timer_period = 0.5 
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def listener1_callback(self, msg):
        if msg.data:
            try:
                BOARD.digital[4].write(0)
                BOARD.digital[12].write(0)
                self.valve1 = True
            except:
                print("IOError\n")
        else:
            try:
                BOARD.digital[4].write(1)
                self.valve1 = False
                if self.valve2 == False:
                    BOARD.digital[12].write(1)
            except:
                print("IOError\n")

    def listener2_callback(self, msg):
        if msg.data:
            BOARD.digital[2].write(0)
            BOARD.digital[12].write(0)
            self.valve2 = True
        else:
            BOARD.digital[2].write(1)
            self.valve2 = False
            if self.valve1 == False:
                BOARD.digital[12].write(1)

    def timer_callback(self):
        msg = Bool()
        msg.data = True
        self.publisher_.publish(msg)

class PublisherExit(Node):

    def __init__(self):
        super().__init__('exit')
        self.publisher1_ = self.create_publisher(Bool, 'disinfection_ardu_ros_pub', 1)
        msg = Bool()
        msg.data = False
        self.publisher1_.publish(msg)

def main(args=None):
    BOARD.digital[12].write(1)
    BOARD.digital[2].write(1)
    BOARD.digital[4].write(1)
    rclpy.init(args=args)
    subscriber = Subscriber()

    try:
        rclpy.spin(subscriber)
    except KeyboardInterrupt:
        publisherExit = PublisherExit()
        rclpy.spin_once
        BOARD.digital[12].write(1)
        BOARD.digital[2].write(1)
        BOARD.digital[4].write(1)
        BOARD.exit()
        print("Communication successfully end\n")
    except IOError:
        print("Communication unexpectedly end\n")    


if __name__ == '__main__':
    main()