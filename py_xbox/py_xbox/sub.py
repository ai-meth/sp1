import rclpy
from rclpy.node import Node

from sensor_msgs.msg import Joy
from std_msgs.msg import Int64, Bool
from step_interfaces.msg import ChassisStatus

from .XOneSButtonDefinitions import XPad 

import json
import serial
import time

SER = serial.Serial("/dev/ttyACM1", baudrate=9600, timeout=2.5)
data = 'a'
SER.write(data.encode('ascii'))
time.sleep(5)


class Subscriber(Node):

    def __init__(self):
        super().__init__('subscriber')
        self.subscription = self.create_subscription(Joy, 'joy', self.listener_callback, 1)
        self.subscription  
        self.pad = XPad()

        self.turnRatioPwm = Int64()
        self.velocityPwm = Int64()
        self.threshold = 0.3

        self.max_speed = [0, 30, 60, 90]
        self.max_speed_rotate = [0, 30, 50, 70]

        self.speed = 0
        self.speed_rotate = 0

        self.block = True
        self.block_btn = 0

        self.publisher1_ = self.create_publisher(ChassisStatus, 'chasiss_block_pub', 1)
        timer_period = 0.5 
        self.timer1 = self.create_timer(timer_period, self.timer_callback)

        self.publisher2_ = self.create_publisher(Bool, 'chassis_ros_pub', 1)
        self.timer2 = self.create_timer(timer_period, self.chassis_status_callback)

        self.get_logger().info('Chassis Node working correctly')

    def listener_callback(self, msg):
        rumble = False

        if msg.buttons[self.pad.XPadButtons["LB"]] and msg.buttons[self.pad.XPadButtons["RB"]]:
            self.block_btn += 1
        elif self.block_btn != 20:
            self.block_btn = 0

        if msg.buttons[self.pad.XPadButtons["LB"]] and not msg.buttons[self.pad.XPadButtons["RB"]] and not self.block:
            self.block = True
            self.block_btn = 0
            rumble = True
        elif self.block_btn == 20 and self.block:
            self.block = False
            rumble = True

        if self.block == False:
            straight = msg.axes[self.pad.XPadAxes["SLY"]]
            turn = msg.axes[self.pad.XPadAxes["SLX"]]

            if msg.buttons[self.pad.XPadButtons["B"]]:
                self.speed = self.max_speed[0]
                self.speed_rotate = self.max_speed_rotate[0]
                self.chassis_stop()
            else:
                if msg.buttons[self.pad.XPadButtons["A"]]:
                    self.speed = self.max_speed[1]
                    self.speed_rotate = self.max_speed_rotate[1]
                elif msg.buttons[self.pad.XPadButtons["X"]]:
                    self.speed = self.max_speed[2]
                    self.speed_rotate = self.max_speed_rotate[2]
                elif msg.buttons[self.pad.XPadButtons["Y"]]:
                    self.speed = self.max_speed[3]
                    self.speed_rotate = self.max_speed_rotate[3]
        
                self.velocityPwm = self.translate(straight, -1, 1, -255, 255)
                self.turnRatioPwm = self.translate(turn, -1, 1, -255, 255)

                self.chassis_control(self.velocityPwm, self.turnRatioPwm)
        else:
            self.chassis_stop()

        if rumble:
            self.pad.rumble(500)

    def translate(self, value, leftMin, leftMax, rightMin, rightMax):
        # Figure out how 'wide' each range is
        leftSpan = leftMax - leftMin
        rightSpan = rightMax - rightMin

        # Convert the left range into a 0-1 range (float)
        valueScaled = float(value - leftMin) / float(leftSpan)

        # Convert the 0-1 range into a value in the right range.
        return rightMin + (valueScaled * rightSpan)

    def chassis_control(self, velocity, turnRatio):
        limit_work = 0
        limit_work2 = 2

        motor_left = 0
        motor_right = 0
        left_motor_speed = 0
        right_motor_speed = 0

        vel_abs = abs(velocity)
        vel_gen_abs = self.translate(vel_abs, 0, 255, 0, self.speed)

        dir_abs = abs(turnRatio)
        dir_gen_abs = self.translate(dir_abs, 0, 255, 0, self.speed)

        speed_d = self.translate(dir_abs, 0, 255, 0, vel_gen_abs)
        speed_v = self.translate(vel_abs, 0, 255, 0, dir_gen_abs)
        vel_gen_abs_rotate = self.translate(vel_abs, 0, 255, 0, self.speed_rotate)


        if (velocity > limit_work) and (turnRatio > limit_work) and (velocity >= turnRatio):
            left_motor_speed = vel_gen_abs
            right_motor_speed = vel_gen_abs - speed_d
            motor_left = 1
            motor_right = 1
        elif (velocity >= limit_work) and (turnRatio >= limit_work) and (velocity < turnRatio):
            left_motor_speed = dir_gen_abs
            right_motor_speed = dir_gen_abs - speed_v
            motor_left = 0
            motor_right = 1 
        elif (velocity < -limit_work) and (turnRatio > limit_work) and (turnRatio >= abs(velocity)):
            left_motor_speed = dir_gen_abs - speed_v
            right_motor_speed = dir_gen_abs
            motor_left = 0
            motor_right = 1
        elif (velocity <= -limit_work) and (turnRatio >= limit_work) and (abs(velocity) > turnRatio):
            left_motor_speed = vel_gen_abs - speed_d
            right_motor_speed = vel_gen_abs
            motor_left = 0
            motor_right = 0
        elif (velocity < -limit_work) and (turnRatio < -limit_work) and (abs(velocity) >= abs(turnRatio)):
            left_motor_speed = vel_gen_abs
            right_motor_speed = vel_gen_abs - speed_d
            motor_left = 0
            motor_right = 0
        elif (velocity <= -limit_work) and (turnRatio <= -limit_work) and (abs(turnRatio) > abs(velocity)):
            left_motor_speed = dir_gen_abs
            right_motor_speed = dir_gen_abs - speed_v
            motor_left = 1
            motor_right = 0 
        elif (velocity > limit_work) and (turnRatio < -limit_work) and (abs(turnRatio) >= velocity):
            left_motor_speed = dir_gen_abs - speed_v
            right_motor_speed = dir_gen_abs
            motor_left = 1
            motor_right = 0
        elif (velocity >= limit_work) and (turnRatio <= -limit_work) and (abs(velocity) > turnRatio):
            left_motor_speed = vel_gen_abs - speed_d
            right_motor_speed = vel_gen_abs
            motor_left = 1
            motor_right = 1

        data = {}

        data["block"] = self.block
        
        if (velocity >= -limit_work2) and (velocity <= limit_work2) and (turnRatio >= -limit_work2) and (turnRatio <= limit_work2):
            data["Motor_left_L_EN"] = 0
            data["Motor_left_R_EN"] = 0
            data["Motor_right_L_EN"] = 0
            data["Motor_right_R_EN"] = 0

            data["Motor_left_LPWM"] = 0
            data["Motor_left_RPWM"] = 0
            data["Motor_right_LPWM"] = 0
            data["Motor_right_RPWM"] = 0
        else:
            data["Motor_left_L_EN"] = 1
            data["Motor_left_R_EN"] = 1
            data["Motor_right_L_EN"] = 1
            data["Motor_right_R_EN"] = 1
            

        if motor_left:
            data["Motor_left_LPWM"] = int(left_motor_speed)
            data["Motor_left_RPWM"] = 0
        else:
            data["Motor_left_LPWM"] = 0
            data["Motor_left_RPWM"] = int(left_motor_speed)
                
        if motor_right:
            data["Motor_right_LPWM"] = int(right_motor_speed)
            data["Motor_right_RPWM"] = 0
        else:
            data["Motor_right_LPWM"] = 0
            data["Motor_right_RPWM"] = int(right_motor_speed)

        data = json.dumps(data)
        if SER.isOpen():
            SER.write(data.encode('ascii'))
            SER.flush()

    @staticmethod
    def chassis_stop():
        data = {}

        data["block"] = True

        data["Motor_left_L_EN"] = 0
        data["Motor_left_R_EN"] = 0
        data["Motor_right_L_EN"] = 0
        data["Motor_right_R_EN"] = 0

        data["Motor_left_LPWM"] = 0
        data["Motor_left_RPWM"] = 0
        data["Motor_right_LPWM"] = 0
        data["Motor_right_RPWM"] = 0

        data = json.dumps(data)
        if SER.isOpen():
            SER.write(data.encode('ascii'))
            SER.flush()

    def timer_callback(self):
        msg = ChassisStatus()
        msg.system_lock = self.block
        msg.speed = int(self.speed)
        msg.speed_rotate = self.speed_rotate
        self.publisher1_.publish(msg)

    def chassis_status_callback(self):
        msg = Bool()
        msg.data = True
        self.publisher2_.publish(msg)

class PublisherExit(Node):

    def __init__(self):
        super().__init__('exit')
        self.publisher1_ = self.create_publisher(ChassisStatus, 'chasiss_block_pub', 1)
        msg = ChassisStatus()
        msg.system_lock = True
        msg.speed = 0
        msg.speed_rotate = 0
        self.publisher1_.publish(msg)

        Subscriber.chassis_stop()

        self.publisher1_ = self.create_publisher(Bool, 'chassis_ros_pub', 1)
        msg = Bool()
        msg.data = False
        self.publisher1_.publish(msg)

def main(args=None):
    rclpy.init(args=args)
    subscriber = Subscriber()

    try:
        rclpy.spin(subscriber)
    except KeyboardInterrupt:
        publisherExit = PublisherExit()
        rclpy.spin_once
        SER.close()
        print("Communication successfully end\n")
    except IOError:
        print("Communication unexpectedly end\n")    


if __name__ == '__main__':
    main()