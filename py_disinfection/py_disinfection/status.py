import rclpy
from rclpy.node import Node
import glob

from std_msgs.msg import Bool

class Publisher(Node):

    def __init__(self):
        super().__init__('pub')
        self.publisher1_ = self.create_publisher(Bool, 'disinfection_ardu_pub', 1)
        timer_period = 0.5 
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.status = False
    
    def __del__(self):
        msg = Bool()
        msg.data = False
        self.publisher1_.publish(msg)

    def timer_callback(self):
        msg = Bool()
        if glob.glob('/dev/ttyACM0'):
            self.status = True
        else:
            self.status = False

        msg = Bool()
        msg.data = self.status
        self.publisher1_.publish(msg)

class PublisherExit(Node):

    def __init__(self):
        super().__init__('exit')
        self.publisher1_ = self.create_publisher(Bool, 'disinfection_ardu_pub', 1)
        msg = Bool()
        msg.data = False
        self.publisher1_.publish(msg)

def main(args=None):

    rclpy.init(args=args)
    publisher = Publisher()

    try:
        rclpy.spin(publisher)
    except KeyboardInterrupt:
        publisherExit = PublisherExit()
        rclpy.spin_once
        print("Communication successfully end\n")
    except IOError:
        print("Communication unexpectedly end\n")
    



if __name__ == '__main__':
    main()